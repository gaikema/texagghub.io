# TexAgg.github.io

This is the repository for my [website](http://mattgaikema.com/).
The old one was [here](http://texagg.github.io/).
I decided to rewrite it in PHP.

---

## Server Information

The site is hosted through Bluehost,
and the server has PHP 5.4 installed.